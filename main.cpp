#include "gc_pointer.h"
#include "LeakTester.h"

int main() {

  Pointer<int> p = new int(16);
  p.showlist();
  
  Pointer<int> q = new int(21);
  q.showlist();

  p = q;
  p.showlist();


  Pointer<int, 3> r = new int[3] {1, 2, 3}; 
  r.showlist();
	
  Pointer<int, 3> s = new int[3] {4, 5, 6}; 
  s.showlist();

  r = s;

  std::cout << "r: ";
  r.showlist();

  std::cout << "s: ";
  s.showlist();

  return 0;
}
