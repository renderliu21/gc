
This is an implementation of a C++ garbage pointer using reference counting, one of the oldest and simplest
garbage collection techniques.

A reference count field is added to
each heap object. It counts how many
references to the heap object exist.
When an object’s reference count
reaches zero, it is garbage and may
collected.

The reference count field is updated
whenever a reference is created,
copied, or destroyed. When a
reference count reaches zero and an
object is collected, all pointers in the
collected object are also be followed
and corresponding reference counts
decremented.


To test, compile and run main.cpp
```
./make
./a.out
```